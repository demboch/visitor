package main;

import visitable.Client;
import visitable.ClientGroup;
import visitable.Order;
import visitable.Product;
import visitor.ReportMaker;

public class Main {

	public static void main(String[] args) {
		Product pralka = new Product();
		pralka.setName("Pralka");
		pralka.setPrice(789.99);

		Product odkurzacz = new Product();
		odkurzacz.setName("Odkurzacz");
		odkurzacz.setPrice(359.99);

		Order order1 = new Order();
		order1.setNumber("1");
		order1.addProduct(odkurzacz);

		Order order2 = new Order();
		order2.setNumber("2");
		order2.addProduct(pralka);

		Client client1 = new Client();
		client1.setNumber("1");
		client1.addOrder(order1);
		client1.addOrder(order2);

		ClientGroup group1 = new ClientGroup();
		group1.setMemberName("Group 1");
		group1.addClient(client1);

		ReportMaker report = new ReportMaker();
		group1.accept(report);
		System.out.println(report.showReport());

	}

}
