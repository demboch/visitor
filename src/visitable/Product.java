package visitable;

import visitor.IVisitor;

public class Product implements IVisitable {

	private String name;
	private double price;

	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

	public String giveReport() {
		StringBuilder string = new StringBuilder();
		string.append("\t\t\t").append(name).append(" - ").append(price).append("\n");
		return string.toString();
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
