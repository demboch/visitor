package visitable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import visitor.IVisitor;

public class Order implements IVisitable {

	private String number;
	private double orderTotalPrice;
	private Date orderDate;
	private List<Product> products = new ArrayList<Product>();
	SimpleDateFormat form = new SimpleDateFormat("dd-MM-YYYY");

	public Order() {
		this.orderDate = new Date();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public double getOrderTotalPrice() {
		return orderTotalPrice;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public void accept(IVisitor visitor) {

		visitor.visit(this);

		if (products != null)
			for (Product p : products) {
				p.accept(visitor);
			}
	}

	public String giveReport() {
		StringBuilder string = new StringBuilder();

		string.append("\tOrder ").append(number).append("\n\t\tTotal price: ").append(orderTotalPrice)
				.append("\n\t\tOrder date: ").append(form.format(orderDate)).append("\n");

		if (products != null) {
			string.append("\t\tProducts: \n");
		}
		return string.toString();
	}

	public void addProduct(Product product) {
		this.products.add(product);
		calculate(product);
	}

	private void calculate(Product product) {
		this.orderTotalPrice += product.getPrice();
	}
}
