package visitable;

import visitor.IVisitor;

public interface IVisitable {

	public String giveReport();

	public void accept(IVisitor visitor);
}
