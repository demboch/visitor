package visitable;

import java.util.List;

import visitor.IVisitor;

import java.util.ArrayList;

public class Client implements IVisitable {

	private String number;
	private List<Order> orders = new ArrayList<Order>();

	public void accept(IVisitor visitor) {

		visitor.visit(this);

		if (orders != null)
			for (Order o : orders) {
				o.accept(visitor);
			}
	}

	public String giveReport() {

		StringBuilder string = new StringBuilder();
		string.append("Client ").append(number).append(":\n");

		if (orders != null) {
			string.append("Orders: \n");
		}
		return string.toString();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void addOrder(Order order) {
		this.orders.add(order);
	}
}
