package visitable;

import java.util.ArrayList;
import java.util.List;

import visitor.IVisitor;

public class ClientGroup implements IVisitable {

	private String memberName;
	private List<Client> clientList = new ArrayList<Client>();

	public void addClient(Client client) {
		this.clientList.add(client);
	}

	public void accept(IVisitor visitor) {

		visitor.visit(this);

		if (clientList != null)
			for (Client c : clientList) {
				c.accept(visitor);
			}
	}

	public String giveReport() {
		StringBuilder string = new StringBuilder();
		string.append(memberName).append(":\n");
		return string.toString();
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
}
