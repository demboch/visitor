package visitor;

import visitable.IVisitable;

public interface IVisitor {

	void visit(IVisitable v);
}
