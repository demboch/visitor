package visitor;

import visitable.Client;
import visitable.Order;
import visitable.Product;
import visitable.IVisitable;

public class ReportMaker implements IVisitor {

	private int numberOfClients = 0;
	private int numberOfOrders = 0;
	private int numberOfProducts = 0;
	private StringBuilder report = new StringBuilder();

	public String showReport() {
		if (numberOfClients != 0)
			report.append("Number of clients: ").append(getNumberOfClients()).append("\n");
		if (numberOfOrders != 0)
			report.append("Number of orders: ").append(getNumberOfOrders()).append("\n");
		if (numberOfProducts != 0)
			report.append("Number of products: ").append(getNumberOfProducts()).append("\n");
		return report.toString();
	}

	public void visit(IVisitable v) {
		report.append(v.giveReport());
		if (v instanceof Client)
			this.numberOfClients++;
		else if (v instanceof Order)
			this.numberOfOrders++;
		else if (v instanceof Product)
			this.numberOfProducts++;
	}

	public int getNumberOfProducts() {
		return numberOfProducts;
	}

	public int getNumberOfOrders() {
		return numberOfOrders;
	}

	public int getNumberOfClients() {
		return numberOfClients;
	}
}
